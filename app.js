const express = require("express");
const app = express();

const tf = require("@tensorflow/tfjs");
const mobilenet = require("@tensorflow-models/mobilenet");
const fs = require("fs");
const formidable = require("formidable");
const bodyParser = require("body-parser");
const image = require("get-image-data");
const Jimp = require("jimp");

app.use(bodyParser.json());

const server = require("http").Server(app);

app.post("/image", (req, res) => {
    console.log('here')
  let form = new formidable.IncomingForm({
    maxFileSize: 10485760, //10MB
  });

  form.parse(req, async (err, fields, files) => {
    if (err) {
        console.log('err', err)
      res.status(500).send("Something went wrong during upload.");
    } else {
      whatIsThis(files.upload.filepath)
        .then((imageClassification) => {
          res.status(200).send({
            classification: imageClassification,
          });
        })
        .catch((err) => {
          console.log(err);
          res
            .status(500)
            .send("Something went wrong while fetching image from URL.");
        });
    }
  });
});

app.post("/image-from-url", async (req, res) => {
    console.log('red', req.body)
  whatIsThis(req.body.url)
    .then((imageClassification) => {
      res.status(200).send({
        classification: imageClassification,
      });
    })
    .catch((err) => {
      console.log(err);
      res
        .status(500)
        .send("Something went wrong while fetching image from URL.");
    });
});


app.post('/imagebase', async (req,res) => {

    var buf = await Buffer.from(req.body.base64, 'base64');
    Jimp.read(buf, async (err, img) => {
      const image = img.bitmap;
      const channelCount = 3;
      const pixelCount = image.width * image.height;
      const vals = new Int32Array(pixelCount * channelCount);

      let pixels = image.exifBuffer;

      for (let i = 0; i < pixelCount; i++) {
        for (let k = 0; k < channelCount; k++) {
          vals[i * channelCount + k] = pixels[i * 4 + k];
        }
      }

      const outputShape = [image.height, image.width, channelCount];

      const input = tf.tensor3d(vals, outputShape, "int32");

      const model = await mobilenet.load();

      let temp = await model.classify(input);

      res.status(200).send({
        classification: temp,
      });
    })
})

function whatIsThis(url) {
  return new Promise((resolve, reject) => {
    image(url, async (err, image) => {
      if (err) {
          console.log("ERR")
        reject(err);
      } else {
        const channelCount = 3;
        const pixelCount = image.width * image.height;
        const vals = new Int32Array(pixelCount * channelCount);

        let pixels = image.data;
        console.log('WHAT pixels', pixels);

        for (let i = 0; i < pixelCount; i++) {
          for (let k = 0; k < channelCount; k++) {
            vals[i * channelCount + k] = pixels[i * 4 + k];
          }
        }

        const outputShape = [image.height, image.width, channelCount];

        const input = tf.tensor3d(vals, outputShape, "int32");

        const model = await mobilenet.load();

        let temp = await model.classify(input);

        resolve(temp);

        
      }
    });
  });
}

const port = process.env.PORT || 3000;

server.listen(port, (req, res) => {
  console.log(`Server is up and running @ port ${port}`);
});